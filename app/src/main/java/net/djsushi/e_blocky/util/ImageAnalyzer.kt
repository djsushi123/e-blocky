package net.djsushi.e_blocky.util

import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage

class ImageAnalyzer : ImageAnalysis.Analyzer {

	@androidx.camera.core.ExperimentalGetImage
	override fun analyze(imageProxy: ImageProxy) {
		val mediaImage = imageProxy.image
		if (mediaImage != null) {
			val image = InputImage.fromMediaImage(mediaImage, imageProxy.imageInfo.rotationDegrees)
			val scanner = BarcodeScanning.getClient()
			scanner.process(image)
				.addOnSuccessListener { barcodes ->
					barcodes[0].displayValue
				}
				.addOnFailureListener {
					// Task failed with an exception
					// ...
				}
				.addOnCompleteListener {
					imageProxy.close()
				}

		}
	}
}