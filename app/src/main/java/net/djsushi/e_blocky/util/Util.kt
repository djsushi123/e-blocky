package net.djsushi.e_blocky.util

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

object Util {

	fun isRidValid(rid: String?): Boolean {
		if (rid == null)
			return false
		return Regex("[A-Z]-[A-Z0-9]{32}").matches(rid)
	}

	fun stringToDatetime(s: String): Long? {
		val date = SimpleDateFormat("dd.MM.yyyy hh:mm:ss", Locale.getDefault()).parse(s)
		return date?.time
	}

	// TODO finish this (I'm not sure how whole number sums are sent by the API)
	fun prettifyPrice(price: String) {
		val (euros, cents) = price.split(".")

	}

}