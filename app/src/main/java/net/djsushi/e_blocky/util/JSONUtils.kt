package net.djsushi.e_blocky.util

import net.djsushi.e_blocky.ui.datamodel.ItemDataModel
import org.json.JSONArray

object JSONUtils {

	fun JSONArrayToItemArray(jsonArray: JSONArray): Array<ItemDataModel> {
		val data = ArrayList<ItemDataModel>()

		for (i in 0 until jsonArray.length()) {
			val item = jsonArray.getJSONObject(i)
			val name = item.optString("name")
			val price = item.optString("price")
			val quantity = item.optString("quantity")
			val vatRate = item.optString("vatRate")
			val itemType = item.optString("itemType")

			data.add(ItemDataModel(itemType, name, price, quantity, vatRate))
		}

		return data.toTypedArray()
	}

}