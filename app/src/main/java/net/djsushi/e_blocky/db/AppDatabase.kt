package net.djsushi.e_blocky.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [Receipt::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
	abstract fun receiptDao(): ReceiptDAO

	companion object {
		lateinit var db: AppDatabase
	}
}