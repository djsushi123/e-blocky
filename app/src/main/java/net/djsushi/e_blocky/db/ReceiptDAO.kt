package net.djsushi.e_blocky.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ReceiptDAO {
	@Query("SELECT * FROM receipt")
	fun getAll(): List<Receipt>

	@Query("SELECT * FROM receipt WHERE rid IN (:receiptIds)")
	fun loadAllByIds(receiptIds: IntArray): List<Receipt>

	@Query("SELECT EXISTS(SELECT * FROM receipt WHERE rid IN (:rid) LIMIT 1)")
	fun existsWithRid(rid: String): Boolean

	@Insert
	fun insertAll(vararg receipts: Receipt)

	@Delete
	fun delete(receipt: Receipt)
}