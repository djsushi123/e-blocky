package net.djsushi.e_blocky.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import net.djsushi.e_blocky.ui.datamodel.ItemDataModel
import net.djsushi.e_blocky.util.JSONUtils
import org.json.JSONObject


class Converters {

	@TypeConverter
	fun itemsToJSON(item: Array<ItemDataModel>?): String {
		return Gson().toJson(item)
	}

	@TypeConverter
	fun JSONToItems(json: String): Array<ItemDataModel>? {

		val itemType = object : TypeToken<Array<ItemDataModel>>() {}.type

		return Gson().fromJson<Array<ItemDataModel>>(json, itemType)
	}

	companion object {
		fun JSONToReceipt(json: JSONObject): Receipt {
			// Receipt ID
			val receipt = json.getJSONObject("receipt")

			val rid = receipt.optString("receiptId")
			val cashRegisterCode: String? = receipt.optString("cashRegisterCode")
			val createDate: String? = receipt.optString("createDate")
			val customerId: String? = receipt.optString("customerId")
			val dic: String? = receipt.optString("dic")
			val exemption: Boolean = receipt.getBoolean("exemption")
			val freeTaxAmount: String? = receipt.optString("freeTaxAmount")
			val icDph: String? = receipt.optString("icDph")
			val ico: String? = receipt.optString("ico")
			val invoiceNumber: String? = receipt.optString("invoiceNumber")
			val issueDate: String? = receipt.optString("issueDate")
			val items: Array<ItemDataModel> = JSONUtils.JSONArrayToItemArray(receipt.getJSONArray("items"))
			val okp: String? = receipt.optString("okp")

			// Organization information
			val org = receipt.getJSONObject("organization")

			val orgBuildingNumber: String? = org.optString("buildingNumber")
			val orgCountry: String? = org.optString("country")
			val orgDic: String? = org.optString("dic")
			val orgIcDph: String? = org.optString("icDph")
			val orgIco: String? = org.optString("ico")
			val orgMunicipality: String? = org.optString("municipality")
			val orgName: String? = org.optString("name")
			val orgPostalCode: String? = org.optString("postalCode")
			val orgPropertyRegistrationNumber: String? = org.optString("propertyRegistrationNumber")
			val orgStreetName: String? = org.optString("streetName")
			val orgVatPayer: Boolean = org.getBoolean("vatPayer")


			val paragon: Boolean = receipt.getBoolean("paragon")
			val paragonNumber: String? = receipt.optString("paragonNumber")
			val pkp: String? = receipt.optString("pkp")
			val receiptNumber: String? = receipt.optString("receiptNumber")
			val taxBaseBasic: String? = receipt.optString("taxBaseBasic")
			val taxBaseReduced: String? = receipt.optString("taxBaseReduced")
			val totalPrice: String? = receipt.optString("totalPrice")
			val type: String? = receipt.optString("type")

			// Unit information
			val unit = receipt.getJSONObject("unit")

			val unitBuildingNumber: String? = unit.optString("buildingNumber")
			val unitCashRegisterCode: String? = unit.optString("cashRegisterCode")
			val unitCountry: String? = unit.optString("country")
			val unitMunicipality: String? = unit.optString("municipality")
			val unitName: String? = unit.optString("name")
			val unitPostalCode: String? = unit.optString("postalCode")
			val unitPropertyRegistrationNumber: String? = unit.optString("propertyRegistrationNumber")
			val unitStreetName: String? = unit.optString("streetName")
			val unitType: String? = unit.optString("unitType")

			// VAT information
			val vatAmountBasic: String? = receipt.optString("vatAmountBasic")
			val vatAmountReduced: String? = receipt.optString("vatAmountReduced")
			val vatRateBasic: String? = receipt.optString("vatRateBasic")

			return Receipt(
				rid, cashRegisterCode, createDate, customerId, dic,
				exemption, freeTaxAmount, icDph, ico, invoiceNumber,
				issueDate, items, okp, orgBuildingNumber, orgCountry, orgDic, orgIcDph, orgIco,
				orgMunicipality, orgName, orgPostalCode, orgPropertyRegistrationNumber,
				orgStreetName, orgVatPayer, paragon, paragonNumber, pkp, receiptNumber,
				taxBaseBasic, taxBaseReduced, totalPrice, type, unitBuildingNumber,
				unitCashRegisterCode, unitCountry, unitMunicipality, unitName, unitPostalCode,
				unitPropertyRegistrationNumber, unitStreetName, unitType, vatAmountBasic,
				vatAmountReduced, vatRateBasic
			)
		}
	}

}