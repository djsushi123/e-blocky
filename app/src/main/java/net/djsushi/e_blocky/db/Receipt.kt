package net.djsushi.e_blocky.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import net.djsushi.e_blocky.ui.datamodel.ItemDataModel

@Entity
data class Receipt(
	// Unique receipt ID
	@PrimaryKey val rid: String,

	@ColumnInfo(name = "cash_register_code") val cashRegisterCode: String?,
	@ColumnInfo(name = "create_date") val createDate: String?,
	@ColumnInfo(name = "customer_id") val customerId: String?,
	@ColumnInfo(name = "dic") val dic: String?,
	@ColumnInfo(name = "exemption") val exemption: Boolean?,
	@ColumnInfo(name = "free_tax_amount") val freeTaxAmount: String?,
	@ColumnInfo(name = "ic_dph") val icDph: String?,
	@ColumnInfo(name = "ico") val ico: String?,
	@ColumnInfo(name = "invoice_number") val invoiceNumber: String?,
	@ColumnInfo(name = "issue_date") val issueDate: String?,
	@ColumnInfo(name = "items") val items: Array<ItemDataModel>?,
	@ColumnInfo(name = "okp") val okp: String?,

	// Organization info
	@ColumnInfo(name = "org_building_number") val orgBuildingNumber: String?,
	@ColumnInfo(name = "org_country") val orgCountry: String?,
	@ColumnInfo(name = "org_dic") val orgDic: String?,
	@ColumnInfo(name = "org_ic_dph") val orgIcDph: String?,
	@ColumnInfo(name = "org_ico") val orgIco: String?,
	@ColumnInfo(name = "org_municipality") val orgMunicipality: String?,
	@ColumnInfo(name = "org_name") val orgName: String?,
	@ColumnInfo(name = "org_postal_code") val orgPostalCode: String?,
	@ColumnInfo(name = "org_property_registration_number") val orgPropertyRegistrationNumber: String?,
	@ColumnInfo(name = "org_street_name") val orgStreetName: String?,
	@ColumnInfo(name = "org_vat_payer") val orgVatPayer: Boolean?,

	@ColumnInfo(name = "paragon") val paragon: Boolean?,
	@ColumnInfo(name = "paragonNumber") val paragonNumber: String?,
	@ColumnInfo(name = "pkp") val pkp: String?,
	@ColumnInfo(name = "receipt_number") val receiptNumber: String?,
	@ColumnInfo(name = "tax_base_basic") val taxBaseBasic: String?,
	@ColumnInfo(name = "tax_base_reduced") val taxBaseReduced: String?,
	@ColumnInfo(name = "total_price") val totalPrice: String?,
	@ColumnInfo(name = "type") val type: String?,

	@ColumnInfo(name = "unit_building_number") val unitBuildingNumber: String?,
	@ColumnInfo(name = "unit_cash_register_code") val unitCashRegisterCode: String?,
	@ColumnInfo(name = "unit_country") val unitCountry: String?,
	@ColumnInfo(name = "unit_municipality") val unitMunicipality: String?,
	@ColumnInfo(name = "unit_name") val unitName: String?,
	@ColumnInfo(name = "unit_postal_code") val unitPostalCode: String?,
	@ColumnInfo(name = "unit_property_registration_number") val unitPropertyRegistrationNumber: String?,
	@ColumnInfo(name = "unit_street_name") val unitStreetName: String?,
	@ColumnInfo(name = "unit_type") val unitType: String?,

	@ColumnInfo(name = "vat_amount_basic") val vatAmountBasic: String?,
	@ColumnInfo(name = "vat_amount_reduced") val vatAmountReduced: String?,
	@ColumnInfo(name = "vat_rate_basic") val vatRateBasic: String?
)
