package net.djsushi.e_blocky.network

import android.content.Context
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

object Network {

	fun getReceiptJSON(context: Context,
	                   rid: String,
	                   responseListener: (response: JSONObject) -> (Unit),
	                   errorListener: (e: VolleyError) -> (Unit)) {

		// Instantiate the RequestQueue.
		val queue = Volley.newRequestQueue(context)
		val url = "https://ekasa.financnasprava.sk/mdu/api/v1/opd/receipt/find"

		val JSONBody = JSONObject("{receiptId: \"$rid\"}")

		// Request a string response from the provided URL.
		val jsonObjectRequest = JsonObjectRequest(
			Request.Method.POST,
			url,
			JSONBody,
			responseListener,
			errorListener)

		// Add the request to the RequestQueue.
		queue.add(jsonObjectRequest)
	}

}