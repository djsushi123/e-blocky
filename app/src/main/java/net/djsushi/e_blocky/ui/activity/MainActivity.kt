package net.djsushi.e_blocky.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.room.Room
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import net.djsushi.e_blocky.R
import net.djsushi.e_blocky.databinding.ActivityMainBinding
import net.djsushi.e_blocky.db.AppDatabase

class MainActivity : AppCompatActivity() {

	private lateinit var binding: ActivityMainBinding

	// coroutine scope
	private val job = Job()
	private val uiScope = CoroutineScope(Dispatchers.Main + job)

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		// initialize database on app start (it's currently in MainActivity onCreate()
		// because it's always the first thing that starts)
		uiScope.launch(Dispatchers.IO) {
			AppDatabase.db = Room.databaseBuilder(
				applicationContext,
				AppDatabase::class.java,
				"app-database"
			).build()
		}


		binding = ActivityMainBinding.inflate(layoutInflater)
		setContentView(binding.root)

		val navView: BottomNavigationView = binding.navView

		val navController = findNavController(R.id.nav_host_fragment_activity_main)
		// Passing each menu ID as a set of Ids because each
		// menu should be considered as top level destinations.
		val appBarConfiguration = AppBarConfiguration(
			setOf(
				R.id.navigation_home, R.id.navigation_receipts, R.id.navigation_notifications
			)
		)
		setupActionBarWithNavController(navController, appBarConfiguration)
		navView.setupWithNavController(navController)
	}
}