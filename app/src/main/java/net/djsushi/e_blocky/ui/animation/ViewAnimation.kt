package net.djsushi.e_blocky.ui.animation

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View
import net.djsushi.e_blocky.ui.home.HomeFragment


object ViewAnimation {

	fun rotateFab(v: View, rotate: Boolean): Boolean {
		v.animate()
			.setDuration(200)
			.rotation(if (rotate) 135f else 0f)

		return rotate
	}

	fun initMiniFab(v: View) {
		v.visibility = View.GONE
		v.translationY = v.height.toFloat()
		v.alpha = 0f
	}

	fun showMiniFab(v: View) {
		v.visibility = View.VISIBLE
		v.alpha = 0f
		v.translationY = v.height.toFloat()
		v.animate()
			.setDuration(200)
			.translationY(0f)
			.alpha(1f)
			.setListener(object : AnimatorListenerAdapter() {
				override fun onAnimationEnd(animation: Animator) {
					super.onAnimationEnd(animation)
					HomeFragment.fabAddIsRotating = false
				}
			}).start()
	}

	fun hideMiniFab(v: View) {
		v.alpha = 1f
		v.translationY = 0f
		v.animate()
				.setDuration(200)
				.translationY(v.height.toFloat())
				.alpha(0f)
				.setListener(object : AnimatorListenerAdapter() {
					override fun onAnimationEnd(animation: Animator) {
						super.onAnimationEnd(animation)
						v.visibility = View.GONE
						HomeFragment.fabAddIsRotating = false
					}
				}).start()
	}
}