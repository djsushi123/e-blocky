package net.djsushi.e_blocky.ui.datamodel

data class ReceiptCardDataModel(
	val name: String,
	val address: String,
	val datetime: String,
	val price: String,
	val items: Array<ItemDataModel>
) {
	var expanded = false
}