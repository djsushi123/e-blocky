package net.djsushi.e_blocky.ui.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import net.djsushi.e_blocky.databinding.LayoutCardReceiptBinding
import net.djsushi.e_blocky.ui.datamodel.ReceiptCardDataModel

class ReceiptRecyclerAdapter(private val receipts: Array<ReceiptCardDataModel>) :
	RecyclerView.Adapter<ReceiptRecyclerAdapter.ReceiptHolder>() {

	lateinit var binding: LayoutCardReceiptBinding

	// coroutine scope
	private val job = Job()
	private val scope = CoroutineScope(Dispatchers.Main + job)

	/**
	 * Provide a reference to the type of views that you are using
	 * (custom ViewHolder).
	 */
	class ReceiptHolder(binding: LayoutCardReceiptBinding)
		: RecyclerView.ViewHolder(binding.root) {

		val txtDatetime = binding.txtDatetime
		val txtAddress = binding.txtAddress
		val txtItemCount = binding.txtItemCount
		val txtName = binding.txtName
		val txtPrice = binding.txtPrice
		val txtItems = binding.txtItems
		val btnExpand = binding.btnExpand
		val btnRetract = binding.btnRetract
		val btnMore = binding.btnMore
		val layoutExpanded = binding.layoutExpanded

	}

	// Create new views (invoked by the layout manager)
	override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ReceiptHolder {
		// Create a new view, which defines the UI of the list item
		binding = LayoutCardReceiptBinding.inflate(LayoutInflater.from(viewGroup.context))

		Log.d("RECYCLE", "creating viewHolder")

		return ReceiptHolder(binding)
	}

	// Replace the contents of a view (invoked by the layout manager)
	@SuppressLint("SetTextI18n")
	override fun onBindViewHolder(receiptHolder: ReceiptHolder, position: Int) {

		if (receipts[position].expanded) {
			var itemsText = ""
			for (item in receipts[position].items) {
				itemsText += "${item.name} - ${item.quantity} - ${item.price}€\n"
			}
			receiptHolder.txtItems.text = itemsText
			receiptHolder.btnExpand.visibility = View.GONE
			receiptHolder.layoutExpanded.visibility = View.VISIBLE
		} else {
			receiptHolder.btnExpand.visibility = View.VISIBLE
			receiptHolder.layoutExpanded.visibility = View.GONE
		}

		receiptHolder.btnExpand.setOnClickListener { _ ->
			// change "expanded" state of ReceiptCard
			receipts[position].expanded = true

			// notify the RecyclerView that the state was changed
			notifyItemChanged(position)
		}

		receiptHolder.btnRetract.setOnClickListener { _ ->
			// change "expanded" state of ReceiptCard
			receipts[position].expanded = false

			// notify the RecyclerView that the state was changed
			notifyItemChanged(position)
		}

		receiptHolder.txtName.text = receipts[position].name
		receiptHolder.txtAddress.text = receipts[position].address
		receiptHolder.txtDatetime.text = receipts[position].datetime
		receiptHolder.txtItemCount.text = receipts[position].items.size.toString() +
				if (receipts[position].items.size == 1) {
					" item"
				} else {
					" items"
				}
		receiptHolder.txtPrice.text = receipts[position].price

		Log.d("RECYCLE", "size = ${receipts.size}, creating with text ${receiptHolder.txtName.text}")
	}

	// Return the size of your dataset (invoked by the layout manager)
	override fun getItemCount() = receipts.size

}