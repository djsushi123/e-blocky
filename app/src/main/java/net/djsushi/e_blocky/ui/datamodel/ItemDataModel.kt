package net.djsushi.e_blocky.ui.datamodel

data class ItemDataModel(
	val itemType: String,
	val name: String,
	val price: String,
	val quantity: String,
	val vatRate: String
)