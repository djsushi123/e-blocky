package net.djsushi.e_blocky.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import net.djsushi.e_blocky.databinding.FragmentHomeBinding
import net.djsushi.e_blocky.db.AppDatabase
import net.djsushi.e_blocky.db.Converters
import net.djsushi.e_blocky.network.Network
import net.djsushi.e_blocky.ui.activity.QrReadActivity
import net.djsushi.e_blocky.ui.animation.ViewAnimation


class HomeFragment : Fragment() {

	private lateinit var homeViewModel: HomeViewModel
	private var _binding: FragmentHomeBinding? = null
	private var isRotated = false

	// coroutine scope
	private val job = Job()
	private val uiScope = CoroutineScope(Dispatchers.Main + job)

	// This property is only valid between onCreateView and
	// onDestroyView.
	private val binding get() = _binding!!

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View {
		homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
		_binding = FragmentHomeBinding.inflate(inflater, container, false)
		val root: View = binding.root

		init()

		return root
	}

	override fun onDestroyView() {
		super.onDestroyView()
		_binding = null
	}

	// initialize everything
	private fun init() {
		initFABs()
	}

	// Initialize all the FABs and give them animations
	private fun initFABs() {
		// setup the main FAB
		binding.fabAdd.setOnClickListener {
			if (fabAddIsRotating) {
				return@setOnClickListener
			}
			isRotated = ViewAnimation.rotateFab(binding.fabAdd, !isRotated)
			fabAddIsRotating = true

			// if the buttons in the "open" state, thus, the little FABs have to be shown
			if (isRotated) {
				ViewAnimation.showMiniFab(binding.fabAddManual)
				ViewAnimation.showMiniFab(binding.fabAddScan)
			} else {
				ViewAnimation.hideMiniFab(binding.fabAddManual)
				ViewAnimation.hideMiniFab(binding.fabAddScan)
			}
		}

		// hide the FAB buttons on fragment create view
		ViewAnimation.initMiniFab(binding.fabAddManual)
		ViewAnimation.initMiniFab(binding.fabAddScan)

		// setup Manual Scan FAB onClickListener
		binding.fabAddManual.setOnClickListener {
			openUIDDialog()
		}

		binding.fabAddScan.setOnClickListener {
			val i = Intent(requireContext(), QrReadActivity::class.java)
			startActivityForResult(i, 69)
		}
	}

	private fun openUIDDialog() {
		val manualInputDialog = ManualInputDialog()
		manualInputDialog.setTargetFragment(this, REQUEST_CODE)
		manualInputDialog.show(parentFragmentManager, "UID input dialog")
	}

	// this method runs when the user clicks "save" in the "manual RID" dialog
	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		// Make sure fragment codes match up
		if (requestCode == REQUEST_CODE || requestCode == 69) {
			// "rid" stands for Receipt ID

			val rid: String = when (requestCode) {
				69 -> data?.getStringExtra("QR_RID").toString()
				REQUEST_CODE -> data?.getStringExtra(RID_BUNDLE_KEY).toString()
				else -> throw Exception("Wrong request code: $requestCode")
			}

			// TODO implement a basic filter for bad RIDs so that the request doesn't have to be sent
//			Log.d("heh", data?.getStringExtra("QR_RID").toString())
			if (rid == "null")
				return


			Network.getReceiptJSON(requireContext(), rid.toString(),
				// If the request succeeds:
				{
					// If the receipt is "null", then the RID was invalid.
					if (it.optJSONObject("receipt") == null) {
						Toast.makeText(
							requireContext(),
							"Invalid receipt ID.",
							Toast.LENGTH_LONG
						).show()
						return@getReceiptJSON
					}

					// If everything is fine up to this point, it's safe to assume
					// that the receipt isn't going to be "null".
					// "it" = response (JSONObject)
					val receipt = Converters.JSONToReceipt(it)
					var toastText: String

					uiScope.launch(Dispatchers.IO) {
						toastText = if (AppDatabase.db.receiptDao().existsWithRid(receipt.rid)) {
							"This receipt is already in the database!"
						} else {
							AppDatabase.db.receiptDao().insertAll(receipt)
							"Added successfully to database!"
						}

						uiScope.launch(Dispatchers.Main) {
							Toast.makeText(
								requireContext(),
								toastText,
								Toast.LENGTH_LONG
							).show()
						}
					}
				},
				// If the request fails
				{
					if (it.networkResponse.statusCode == 415) {
						binding.txtRid.text = "Receipt non existent in database."
					} else {
						binding.txtRid.text = it.toString()
					}

				})

		}
	}

	companion object {
		// request code for ManualInputDialog fragment
		const val REQUEST_CODE = 666
		const val RID_BUNDLE_KEY = "uid"

		var fabAddIsRotating = false
	}
}