package net.djsushi.e_blocky.ui.home

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatDialogFragment
import net.djsushi.e_blocky.databinding.LayoutManualReceiptInputBinding


class ManualInputDialog : AppCompatDialogFragment() {

	private var _binding: LayoutManualReceiptInputBinding? = null
	private val binding get() = _binding!!

	@SuppressLint("InflateParams")
	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

		val builder = AlertDialog.Builder(context)
		_binding = LayoutManualReceiptInputBinding.inflate(LayoutInflater.from(context))

		with (builder) {
			setView(binding.root)
			setTitle("Enter UID")
			setNegativeButton("Cancel", DialogInterface.OnClickListener { _, _ ->

			})
			setPositiveButton("Save", DialogInterface.OnClickListener { _, _ ->
				sendResult(HomeFragment.REQUEST_CODE)
			})
		}

		val dialog = builder.create()

		dialog.setCancelable(true)
		dialog.setCanceledOnTouchOutside(false)

		return dialog
	}

	private fun sendResult(REQUEST_CODE: Int) {
		val intent = Intent()
		intent.putExtra(HomeFragment.RID_BUNDLE_KEY, binding.editRid.text.toString())
		targetFragment!!.onActivityResult(targetRequestCode, REQUEST_CODE, intent)
	}
}