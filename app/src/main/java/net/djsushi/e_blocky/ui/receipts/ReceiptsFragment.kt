package net.djsushi.e_blocky.ui.receipts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import net.djsushi.e_blocky.databinding.FragmentReceiptsBinding
import net.djsushi.e_blocky.db.AppDatabase
import net.djsushi.e_blocky.ui.adapter.ReceiptRecyclerAdapter
import net.djsushi.e_blocky.ui.datamodel.ReceiptCardDataModel
import net.djsushi.e_blocky.util.Util
import net.djsushi.e_blocky.util.VerticalSpaceItemDecoration


class ReceiptsFragment : Fragment() {

	private lateinit var receiptsViewModel: ReceiptsViewModel
	private var _binding: FragmentReceiptsBinding? = null


	// This property is only valid between onCreateView and
	// onDestroyView.
	private val binding get() = _binding!!


	// coroutine scope
	private val job = Job()
	private val scope = CoroutineScope(Dispatchers.Main + job)


	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View {
		receiptsViewModel = ViewModelProvider(this).get(ReceiptsViewModel::class.java)
		_binding = FragmentReceiptsBinding.inflate(inflater, container, false)
		val root: View = binding.root

		val recycler = binding.recyclerReceipts

//		(recycler.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false

		recycler.setHasFixedSize(true)
		recycler.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)

		val receipts = arrayListOf<ReceiptCardDataModel>()

		scope.launch(Dispatchers.IO) {

			for (receipt in AppDatabase.db.receiptDao().getAll()) {
				val receiptCard = ReceiptCardDataModel(
					receipt.orgName.toString(),
					"${receipt.unitStreetName} ${receipt.unitBuildingNumber}, " +
					"${receipt.unitPostalCode} ${receipt.unitMunicipality}",
					receipt.issueDate.toString(),
					receipt.totalPrice.toString() + "€",
					receipt.items!!
				)
				receipts.add(receiptCard)
			}



			scope.launch(Dispatchers.Main) {
				// sort the array by date and time and then assing it to the RecyclerView
				recycler.adapter = ReceiptRecyclerAdapter(receipts.sortedBy {
					Util.stringToDatetime(it.datetime)
				}.reversed().toTypedArray())
			}
		}



		val spacer = VerticalSpaceItemDecoration(14)
		recycler.addItemDecoration(spacer)

		return root
	}

	override fun onDestroyView() {
		super.onDestroyView()
		_binding = null
	}
}