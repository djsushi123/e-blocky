# E-bločky

This app _will_ be able to scan a QR code on any receipt issued in the Slovak Republic and save them in a database. Currently, the project is in active development.

## Goals
My main goal is to help people never lose a receipt ever again. There are a few apps just like this one available on the Google Play Store however I don't like their lack of functionality. I'm planning to add advanced functionality so that the user has full control of his/her receipt database.

## TO-DO list
- [x] ~~Add a FAB with a submenu (manual and automatic RID* input)~~
- [x] ~~Add a manual UID input dialog when the manual RID input button is pressed~~
- [x] ~~Add Room database~~
- [x] ~~Add Automatic database entry creation when using manual RID input~~
- [x] ~~Add QR scanning capabilities~~
- [x] ~~Add a way to show the database contents to the user~~
- [ ] Add a settings screen
- [ ] Add a receipt search function
- [ ] Add a separate receipt info activity
- [ ] Add a way to take a photo of the receipt
- [ ] Add a way to track warranty of items


*RID - Receipt ID
